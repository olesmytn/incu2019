
###################################################
# Parse internal CRC counters of a Nexus
# The students will have to parse the next output, and return a dictionary,
# with the parsed info
# 
# How the text input will look like:
#
# --------------------------------------------------------------------------------
# Port          Align-Err    FCS-Err   Xmit-Err    Rcv-Err  UnderSize OutDiscards
# --------------------------------------------------------------------------------
# Eth1/1                0          0          0          0          0           0
# Eth1/2                0          0          0          0          0           0
# Eth1/3                0          0          0          0          0           0
# Eth1/4                0          0          0          0          0           0
# <snip>
#
# --------------------------------------------------------------------------------
# Port         Single-Col  Multi-Col   Late-Col  Exces-Col  Carri-Sen       Runts
# --------------------------------------------------------------------------------
# Eth1/1                0          0          0          0          0           0
# Eth1/2                0          0          0          0          0           0
# <snip>
#
# --------------------------------------------------------------------------------
# Port          Giants SQETest-Err Deferred-Tx IntMacTx-Er IntMacRx-Er Symbol-Err
# --------------------------------------------------------------------------------
# Eth1/1             0          --           0           0           0          0
# Eth1/2             0          --           0           0           0          0
#
# How the output must look like:
#
# int_dict = {
# 	"Eth1/1": {
# 		"Align-Err": 0,
# 		"FCS-Err": 0,
# 		...
# 	}
# }
###################################################
import re

def get_error_counters(text):
    ports = [] #contains all ports: "Eth1/1, Eth1/2, etc"
    titles = [] #contains titles like "Align-Err", "FCS-Err", etc
    lines = [] #contains the whole line with counters for particular port
    values = [] #contains values of counters 
    titlesValues={}
    interfaces_dict = {} #output dict
    #regex used to find titles
    title = re.compile(r'\b([A-Z][a-zA-Z-]+)\b')
    #regex used to find ports
    port = re.compile(r'[a-zA-Z]+\d+\/?\d*')
    #find all ports
    for i in port.findall(text):
        #if list already contains this port, don't add it
        if i not in ports:
            ports.append(i)
    #find all titles exept of "Port", which is not used in interfaces_dict
    for i in title.findall(text):
        if i !='Port':
            titles.append(i)
    #create regexes for all lines, which contain data for particular port
    for i in ports:
        lines.append(re.compile('(?<={})(.*)(?=\n)'.format(i)))
    #for each port
    for i in range(len(ports)):
        #get values of counters in all lines
        for m in lines[i].finditer(text):
            values += m.group(1).split()
        #create the dict with titles and values of counters
        titlesValues.update(zip(titles[:len(values)], values))
        #create the output
        interfaces_dict.update({ports[i]: titlesValues.copy()})
        titlesValues = {}
        del values[:]
    return interfaces_dict
