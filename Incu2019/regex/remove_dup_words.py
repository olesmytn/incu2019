
###################################################
# Duplicate words 
# The students will have to find the duplicate words in the given text,
# and return the string without duplicates.
#
###################################################
import re


def remove_duplicates(text):
    text_with_no_dup = re.sub(r'\b(\w+)(\s\1\b)+', r'\1',text)
    return text_with_no_dup

