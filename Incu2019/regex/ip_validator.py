
###################################################
# IP address validator 
# details: https://en.wikipedia.org/wiki/IP_address
# Student should enter function on the next lines.
# 
# 
# First function is_valid_ip() should return:
# True if the string inserted is a valid IP address
# or False if the string is not a real IP
# 
# 
# Second function get_ip_class() should return a string:
# "X is a class Y IP" or "X is classless IP" (without the ")
# where X represent the IP string, 
# and Y represent the class type: A, B, C, D, E
# ref: http://www.cloudtacker.com/Styles/IP_Address_Classes_and_Representation.png
# Note: if an IP address is not valid, the function should return
# "X is not a valid IP address"
###################################################

import re

def is_valid_ip(ip):
    regex = re.compile('^((0|1\d{1,2}|2[0-4][0-9]|25[0-5]|[1-9][0-9]?)\.){3}(0|1\d{1,2}|2[0-4][0-9]|25[0-5]|[1-9][0-9]?)$')
    if regex.match(ip) is None:
        valid_flag = False
    else:
        valid_flag = True
    return valid_flag

def get_ip_class(ip):
    regex = re.compile('^\d{1,3}(?=\.)')
    patternsDic = {'^0':'A','^10':'B','^110':'C','^1110':'D','^1111':'E'}
    if is_valid_ip(ip):
        FirstByteDec = regex.findall(ip)
        FirstByteBin = format(int(FirstByteDec[0]),'08b')
        for key in patternsDic:
           if re.match(key,FirstByteBin) is not None:
                return_text = '{} is a class {} IP'.format(ip, patternsDic[key])
    else:
        return_text = '{} is not a valid IP address'.format(ip)
    return return_text
