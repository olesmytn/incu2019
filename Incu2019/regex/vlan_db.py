
###################################################
# Parse vlan status
# 
# Students will need to parse the output of a "show vlan" from a NXOS devices
# and return a dictionary with the parsed info
# 
# How the text input will look like:
# 
# 
# VLAN Name                             Status    Ports
# ---- -------------------------------- --------- -------------------------------
# 1    default                          active    Po213, Eth1/1, Eth1/2, Eth1/3
#                                                 Eth1/4
# 10   Vlan10                           active    Po1, Po10, Po111, Po213, Eth1/2
#                                                 Eth1/5, Eth1/16, Eth1/17
#                                                 Eth1/18, Eth1/49, Eth1/50
# How the output must look like:
# 
# vlan_db = {
# 	"1": {
# 		"Name": "default",
# 		"Status": "active",,
# 		"Ports": ["Po213", "Eth1/1"...]
# 	},
# 	"2":{},
# 	...
# }
#
###################################################
import re

def get_vlan_db(text):
    #Compile the regex, which has 4 groups
    #Group 1 (\d+) matches the VLAN number
    #Group 2 (\w+) matches the VLAN name
    #Group 3 (\w+) matches the VLAN status
    #Group 4 (\w+\d+\/?\d*) matches the VLAN ports
    regx = re.compile('(?m)^\s*(\d+)\s*(\w+)\s*(\w+)\s*|(\w+\d+\/?\d*)')
    #Dict vlan_db contains parsed data.
    #Structure of vlan_db = {vlan_number: {"Name": , "Status": , "Ports": ports[]} }
    vlan_db = {}
    ports = []
    for m in regx.finditer(text):
        #if VLAN number, name and status is found, add it to the dict
        if m.group(1) and m.group(2) and m.group(3):
            vlan_db.update({m.group(1): {"Name": m.group(2), "Status": m.group(3)} })
            #remember the number of this VLAN
            vlanName = m.group(1)
            del ports[:]
        else:
            #while VLAN number, name and status is None and gr4 matches the VLAN ports, add those ports to the list
            ports.append(m.group(4))
            #use remembered VLAN number as key to add list of ports to the dict
            vlan_db[vlanName].update({"Ports": ports.copy()})
    return vlan_db

