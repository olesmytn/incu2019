from flask import Flask
from flask import render_template
from flask import jsonify
from flask import request
from bson.objectid import ObjectId
import json

from flask_pymongo import PyMongo


app = Flask(__name__)

app.config["MONGO_URI"] = "mongodb://127.0.0.1:27017/flask"
mongo = PyMongo(app)
data = mongo.db.AddressBook

#HTML
#Index page
@app.route("/")
def index():
	return render_template('index.html')
	
#List all data from MongoDB to HTML table 
@app.route("/list_all_html")
def list_all_html():
	output = []
	for s in data.find():
		_id = str(s['_id'])
		output.append({'_id':_id,'name': s['name'], 'phone': s['phone'],'email': s['email']})
	return render_template('listall.html', arr=output)

#Submit new element through HTML form
@app.route("/submit", methods=['GET','POST'])
def submit():
	code = ''
	if request.method == 'POST':
		try:
			name = request.form['name']
			phone = request.form['phone_number']
			email = request.form['email_address']
			if name and phone and email:
				new_post_id = data.insert({'name': name, 'phone': phone, 'email': email})
				if new_post_id:
					code = 'Resource is inserted: 200 OK'
			else:
				code = 'Fill in the data \n Resource not found: 404'
		except:
			code = 'Error occured: 500'
	return render_template('submitform.html',code=code)

#JSON
#List all data from MongoDB in JSON
@app.route("/list_all_json")
def list_all_json():
	output = []
	try:
		for s in data.find():
			_id = str(s['_id'])
			output.append({'_id':_id,'name': s['name'], 'phone': s['phone'],'email': s['email']})
		if output:
			code = "200"
		else:
			code = "404"
	except:
		code = "500"
	return jsonify({'AddressBook': output}), code

#Get element by ID
@app.route("/get_by_id/<_id>")
def get_by_id(_id):
	output = []
	try:
		found_elem = data.find_one({'_id': ObjectId(_id)})
		if found_elem:
			output = {'_id': _id, 'name': found_elem['name'], 'phone': found_elem['phone'],'email': found_elem['email']}
			code = "200"
		else:
			code = "No such an object: " + _id + " 404"
	except:
		code="500"
	return jsonify({'result': output}), code
	
#Edit an element
@app.route('/edit/', methods=['PUT'])
def edit():
	try:
		update_data = request.get_json()
		if '_id' in update_data.keys():
			get_id = update_data['_id']
			del update_data['_id']
			for field, content in update_data.items():
				data.update_one({'_id': ObjectId(get_id)},
								 {'$set': {field: content}},)
			update_data.update({'_id': get_id})
			code = 200
		else:
			code = 500
			update_data.clear()
	except:
		code = 404
		update_data.clear()
	return jsonify(update_data), code

#Create new element
@app.route("/create_new_post/", methods=['POST'])
def create_new_post():
	output = []
	try:
		name = request.json['name']
		phone = request.json['phone']
		email = request.json['email']
		new_post_id = data.insert({'name': name, 'phone': phone, 'email': email})
		if new_post_id:
			code = 'Resource is inserted: 200 OK'
		else:
			code = 'Resource Not found: 404'
	except:
		code = 'Error occured: 500'
	return code

#Delete an element
@app.route("/delete/<_id>", methods=['DELETE'])
def delete(_id):
	try:
		delete_elem = data.delete_one({"_id": ObjectId(_id)})
		if delete_elem.deleted_count > 0:
			code = 'Resource is deleted: 200 OK'
		else:
			code = 'Resource Not found: 404'
	except:
		code = 'Error occured: 500'
	return code

app.run(debug = True, port=7676)
