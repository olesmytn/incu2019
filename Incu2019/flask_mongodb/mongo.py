from pymongo import MongoClient
from bson.json_util import dumps

import json
import pymongo

def get_mongo():
	client = MongoClient("127.0.0.1", 27017)
	db = client["incubator"]
	mycol = db.AddressBook
	return mycol

def insert_new_entry(insert_data):
        mycol = get_mongo()
        mycol.insert_one(insert_data)
        
def update_entry(firstname,lastname,update_data):
        mycol = get_mongo()
        mycol.update_one({"firstname": firstname, "lastname": lastname},
                         {"$set":update_data})

def display_entries(firstname=None,lastname=None):
        mycol = get_mongo()
        firstname_rgx = "{}.*".format(firstname)
        lastname_rgx = "{}.*".format(lastname)
        filter1 = {"$and":
                  [{"firstname":{"$regex":firstname_rgx}},
                   {"lastname":{"$regex": lastname_rgx}}]}
        filter2 = {"$or":
                  [{"firstname":{"$regex":firstname_rgx}},
                   {"lastname":{"$regex": lastname_rgx}}]}
        if firstname == None and lastname == None:
                doc = mycol.find().sort("lastname",pymongo.ASCENDING)
        elif firstname == None or lastname == None:
                doc = mycol.find(filter2)
        else:
                doc = mycol.find(filter1)        
        for i in doc:
                print(i)
                
def delete_entries(firstname=None,lastname=None):
        mycol = get_mongo()
        mycol.delete_many({"$and":
                          [{"firstname":firstname},
                          {"lastname":lastname}]})
