
import mongoDB

mongoDB.insert_new_entry({'firstname': 'Tony', 'lastname': 'Nostry', 'email': 't.nost@gmail.com', 'home phone': '345688'})
mongoDB.insert_new_entry({'firstname': 'Micheal', 'lastname': 'Liberman', 'email': 'm.liber@gmail.com', 'work phone': '883311'})
mongoDB.insert_new_entry({'firstname': 'Olga', 'lastname': 'Kozyra', 'email': 'koz@gmail.com','home phone':'556677','work phone':'775566'})
mongoDB.insert_new_entry({'firstname': 'Tomas', 'lastname': 'Amber', 'email': 'amber@gmail.com'})
mongoDB.insert_new_entry({'firstname': 'Tom', 'lastname': 'Arman', 'email': 'arm@gmail.com'})
mongoDB.insert_new_entry({'firstname': 'Nicolas', 'lastname': 'Lorson', 'email': 'n.lorson@gmail.com'})
mongoDB.insert_new_entry({'firstname': 'Lucy', 'lastname': 'Fisher', 'email': 'fish@gmail.com'})
mongoDB.insert_new_entry({'firstname': 'Tom', 'lastname': 'Smith', 'email': 'smith@gmail.com', 'home phone': '556625'})
mongoDB.insert_new_entry({'firstname': 'Amelie', 'lastname': 'Backer', 'email': 'abacker@gmail.com'})
mongoDB.insert_new_entry({'firstname': 'Micheal', 'lastname': 'Kors', 'email': 'm.kors@gmail.com'})
mongoDB.insert_new_entry({'firstname': 'Nikkie', 'lastname': 'Milch', 'email': 'n.mlc@gmail.com'})
mongoDB.insert_new_entry({'firstname': 'Micky', 'lastname': 'Alcher', 'email': 'm.al@gmail.com'})
mongoDB.insert_new_entry({'firstname': 'Nicolas', 'lastname': 'Meisol', 'email': 'n.meisol@gmail.com'})
mongoDB.insert_new_entry({'firstname': 'Lucy', 'lastname': 'Meierson', 'email': 'lucy@gmail.com'})
mongoDB.insert_new_entry({'firstname': 'Miranda', 'lastname': 'Kerr', 'email': 'm.kerr@gmail.com'})

#display entries befor update

print('\n~~~~~~~~~~~Entries before update~~~~~~~~~~~\n')
mongoDB.display_entries()

#update entry
update_data = {'email':'tom.smith@gmail.com', 'work phone': '333333'}
mongoDB.update_entry('Tom','Smith',update_data)

print('\n~~~~~~~~~~~Entries after update~~~~~~~~~~~\n')
mongoDB.display_entries()

print('\n~~~~~~~~~~~Entries where firstname starts with "Ni"\n')
mongoDB.display_entries('Ni')

print('\n~~~~~~~~~~~Entries where lastname starts with "K"\n')
mongoDB.display_entries(lastname = 'K')

print('\n~~~~~~~~~~~Entries where firstname starts with "To" and lastname with "A"\n')
mongoDB.display_entries('To','A')

mongoDB.delete_entries('Tom', 'Smith')
print('\n~~~~~~~~~~~Entries after deleting~~~~~~~~~~~\n')
mongoDB.display_entries()
