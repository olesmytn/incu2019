from nxostoolkit import nexus

a = nexus()
print(a.version, a.platform, sep="\n")
a.authenticate('admin','Admin_1234!')
status = a.get_interface_status('eth1/1')
print(status)
a.configure_interface_desc('eth1/1','descr_test')
print(a.version, a.platform, sep="\n")
