import requests
import json

class nexus:
     
    version = 'Cannot get the NXOS version. Authenticate first.'
    platform = 'Cannot get the NX Platform. Authenticate first.'

    def __init__(self):
        self.cookies = ''
        self.switch = {"ip": "sbx-nxos-mgmt.cisco.com",
                       "port": "443"}

    def get_version(self):
        """
        Used to get the version on NXOS
        """
        url = 'http://{}/api/mo/sys/showversion.json'.format(self.switch['ip'])
        http_header = {'content-type': 'application/json'}
        response = requests.get(url,
                                headers = http_header,
                                cookies = self.cookies)
        response = response.json()    
        self.version = 'Current version of NXOS: {}'.format(response['imdata'][0]['sysmgrShowVersion']['attributes']['nxosVersion'])
      
    def get_platform(self):
        """
        Used to get the platform description
        """
        #because there is no field chassis_id in showversion.json response, I used ch.json instead
        url = 'http://{}/api/mo/sys/ch.json'.format(self.switch['ip'])
        http_header = {'content-type': 'application/json'}
        response = requests.get(url,
                                headers = http_header,
                                cookies = self.cookies)
        response = response.json()
        self.platform = 'Platform: {}'.format(response['imdata'][0]['eqptCh']['attributes']['descr'])

    def authenticate(self, user, pwd):
        """
        The method is used to authenticate to a switch
        """
        #Define JSON header
        json_headers = {'Content-Type': 'application/json'}
        #Create URL to call the authentication method
        url = 'http://{}/api/aaaLogin.json'.format(self.switch['ip'])
        #Create payload of login message
        payload = {"aaaUser": {
                       "attributes": {
                           "name": user,
                           "pwd": pwd}}}
        #Post the request
        auth_response = requests.post(url,
                                      data = json.dumps(payload),
                                      headers = json_headers)
        #Remember cookies, later it is used to get the interface status and to configure the descr
        #(eliminates '403 Forbidden' HTTP response)
        auth = auth_response.json()
        token = auth['imdata'][0]['aaaLogin']['attributes']['token']
        self.cookies = {'APIC-Cookie': token}
        #after authentication, we can reassing the version and platform
        version = self.get_version()
        platform = self.get_platform()
        
    def create_int_name(self, interface):
        """
        Create a proper name of the interface id, which can be used for HTTP requests
        e.g Eth1/1, Ethernet 1/1, ether 1/1 etc. -> eth1/1
        """
        #Delete spaces at the beginning and end of the string
        interface = interface.strip()
        #Convert string to lower case
        interface = interface.lower()
        #Get 3 first chars (int type: eth) + last  3 chars (int number: 1/1)
        interface = interface[:3] + interface[-3:]
        return interface
        
    def get_interface_status(self, if_name):
        """
        The method gets the status of the interface
        """
        #Get the proper int name
        if_name = self.create_int_name(if_name)
        #Define the header of request
        http_header = {'content-type': 'application/json'}
        #Define the URL of get request
        url = 'http://{}/api/mo/sys/intf/phys-[{}].json'.format(self.switch['ip'],if_name)
        #Send the GET request
        response = requests.get(url,
                                headers = http_header,
                                cookies = self.cookies)
        #Search for the status of the interface in HTTP response
        int_response = response.json()
        int_status = int_response['imdata'][0]['l1PhysIf']['attributes']['adminSt']
        #check the status of the int and return the answer (up, down, unknown)
        if int_status == 'up':
            return ('Status of the interface {} is up'.format(if_name))
        elif int_status == 'down':
            return ('Status of the interface {} is down'.format(if_name))
        else:
            return ('Status of the interface {} is unknown'.format(if_name))

    def configure_interface_desc(self, if_name, description):
        """
        Configures the interface description
        """
        #create the proper name of the interface
        if_name = self.create_int_name(if_name)
        #define header, url and payload for HTTP POST
        http_header = {'content-type': 'application/json'}
        url = 'http://{}/api/mo/sys/intf.json'.format(self.switch['ip'])
        payload = {"l1PhysIf" : {
                        "attributes" : {
                                 "id" : if_name,
                                 "descr" : description }}}
        #Send HTTP Post and get the response
        response = requests.post(url,
                                data = json.dumps(payload),
                                headers = http_header,
                                cookies = self.cookies)
